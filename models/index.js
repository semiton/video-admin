const path = require('path');
/**
 * @param {Sequelize} sequelize
 */
module.exports = (sequelize) => {
    // Import models
    let models = {
        Author: sequelize.import(path.resolve(__dirname, 'author.js')),
        Category: sequelize.import(path.resolve(__dirname, 'category.js')),
        Video: sequelize.import(path.resolve(__dirname, 'video.js')),
        Tag: sequelize.import(path.resolve(__dirname, 'tag.js')),
        Comment: sequelize.import(path.resolve(__dirname, 'comment.js')),
        Admin: sequelize.import(path.resolve(__dirname, 'admin.js')),
    };

    // Associations

    // Video has one Category (belongsTo)
    models.Video.belongsTo(models.Category, {
        onDelete: 'cascade',
        foreignKey: {
            field: 'categoryId',
            allowNull: false,
        }
    });
    // Video has one Author (belongsTo)
    models.Video.belongsTo(models.Author, {
        onDelete: 'cascade',
        foreignKey: {
            field: 'authorId',
            allowNull: false,
        }
    });
    // Comment has one Video (belongsTo)
    models.Comment.belongsTo(models.Video, {
        onDelete: 'cascade',
        foreignKey: {
            field: 'videoId',
            allowNull: false,
        }
    });
    // Author has many videos (oneToMany or hasMany)
    models.Author.hasMany(models.Video, {
        as: 'Videos'
    });
    // Category has many videos (oneToMany or hasMany)
    models.Category.hasMany(models.Video, {
        as: 'Videos'
    });
    // Video has many comments (oneToMany or hasMany)
    models.Video.hasMany(models.Comment, {
        as: 'Comments'
    });
    // Video has many comments (belongsToMany or manyToMany)
    models.Video.belongsToMany(models.Tag, {
        as: 'Tags',
        through: 'videoTags'
    });

    return models;
};