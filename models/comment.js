/**
 * @param {Sequelize} sequelize
 * @param {Sequelize.DataTypes} DataTypes
 */
module.exports = (sequelize, DataTypes) => {
    return sequelize.define("comment", {
        username: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        message: {
            type: DataTypes.TEXT,
            allowNull: false,
        }
    })
};