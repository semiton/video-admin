/**
 * @param {Sequelize} sequelize
 * @param {Sequelize.DataTypes} DataTypes
 */
module.exports = (sequelize, DataTypes) => {
    return sequelize.define("video", {
        status: {
            type: DataTypes.STRING
        },
        title: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.TEXT
        }
    })
};