const { URLSearchParams, URL } = require('url');
const config = require('config');

module.exports = () => {
    return {
        asset: (assetPath) => {
            let url = new URL(
                [config.get('assets.baseUrl'), assetPath].join(''),
                config.get('domain.baseUrl').toString()
            );

            url.searchParams.append('v', config.get('assets.version'));

            return url.toString();
        },

        url: (path, params = {}) => {
            let urlParams = {};

            Object.keys(params).forEach(name => {
                if (name === '_keys') {
                    return;
                }
                if (name.match(/^:/) === null) {
                    urlParams[name] = params[name];
                    return;
                }

                path = path.split(name).join(String(params[name]));
            });

            let url = new URL(
                Object.keys(urlParams).length === 0 ? path : [path, (new URLSearchParams(urlParams)).toString()].join('?'),
                config.get('domain.baseUrl').toString()
            );

            return url.toString();
        }
    };
};
