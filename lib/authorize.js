const config = require('config');
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    let redirect = () => {
        if (req.xhr) {
            return next({
                status: 401,
                messsage: 'Not authorized'
            });
        }

        return res.redirect('/login');
    };

    if (req.session.token) {
        return jwt.verify(req.session.token, config.get('session.secret'), (error, user) => {
            if (!error) {
                res.user = user;
                return next();
            }

            return redirect();
        });
    }

    return redirect();
};
