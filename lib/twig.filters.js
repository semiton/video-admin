module.exports = Twig => {
    Twig.extendFilter('ceil', value => {
        return Math.ceil(value);
    });

    Twig.extendFilter('url_decode', value => {
        return decodeURI(value);
    });
};
