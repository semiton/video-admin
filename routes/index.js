const express = require('express');
const router = express.Router();
const authorize = require('../lib/authorize');

router.use('/login', require('./login'));
router.use('/logout', require('./logout'));

router.use('/authors', authorize, require('./authors'));
router.use('/categories', authorize, require('./categories'));
router.use('/videos', authorize, require('./videos'));
router.use('/tags', authorize, require('./tags'));
router.use('/comments', authorize, require('./comments'));

router.use('/', authorize, require('./main'));

module.exports = router;