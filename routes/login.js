const config = require('config');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.get('/', (req, res, next) => {
    res.render('login');
});

router.post('/', (req, res, next) => {
    let username = String(req.body.username || '').trim(),
        password = String(req.body.password || '').trim(),
        Admin = req.app.get('models').Admin;

    if (username.length === 0 || password.length === 0) {
        return res.redirect('/login');
    }

    return Admin
        .findOne({
            where: {
                username: username,
                password: Admin.hashPassword(password)
            }
        })
        .then(user => {
            if (!user) {
                req.flash('error', 'Incorrect username or password.');
                return res.redirect('/login');
            }

            return jwt.sign(user.dataValues, config.get('session.secret'), { expiresIn: config.get('session.cookie.maxAge') }, (error, token) => {
                if (error) {
                    req.flash('error', error.toString());
                    return res.redirect('/login');
                }

                req.session.token = token;
                return res.redirect('/');
            });
        })
        .catch(error => {
            req.flash('error', error.toString());
            return res.redirect('/login');
        });
});

module.exports = router;