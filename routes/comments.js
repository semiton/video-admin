const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        videoId = req.query.video || null,
        limit = req.query.limit || 10,
        page = req.query.page || 1,
        offset = (page - 1) * limit;

    if (!videoId || (String(videoId).trim().length === 0)) {
        videoId = null;
    }

    let where = {};
    if (videoId !== null) {
        where = {
            videoId: videoId
        };
    }

    let getVideo = () => {
        return new Promise((resolve, reject) => {
            if (videoId === null) {
                return resolve(videoId);
            }

            return models.Video
                .findById(videoId)
                .then(resolve)
                .catch(reject);
        });
    };

    return getVideo()
        .then(video => {
            return models.Comment
                .findAndCountAll({
                    offset: offset,
                    limit: limit,
                    where: where,
                    include: [{
                        model: models.Video,
                        as: 'video',
                        include: [{
                            model: models.Author,
                            as: 'author'
                        }, {
                            model: models.Category,
                            as: 'category'
                        }]
                    }]
                })
                .then(result => {
                    res.render('comments/index', {
                        video: video,
                        rows: result.rows,
                        count: result.count,
                        page: page,
                        offset: offset,
                        limit: limit
                    });
                })
                .catch(next);
        })
        .catch(next);
});

router.get('/:id(\\d+)/remove', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id'),
        returnUrl = req.param('return') || '';

    returnUrl = decodeURI(returnUrl);

    return models.Comment
        .destroy({
            where: {
                id: id
            }
        })
        .then(removed => {
            if (removed) {
                req.flash('success', 'Comment has removed successfully!');
            }
            return res.redirect(returnUrl);
        })
        .catch(error => {
            req.flash('error', error.toString());
            return res.redirect(returnUrl);
        });
});

module.exports = router;
