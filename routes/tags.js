const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = req.query.limit || 10,
        page = req.query.page || 1,
        offset = (page - 1) * limit;

    models.Tag
        .findAndCountAll({
            offset: offset,
            limit: limit
        })
        .then(result => {
            res.render('tags/index', {
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });
        })
        .catch(next);
});

router.get('/search', (req, res, next) => {
    let models = req.app.get('models'),
        limit = req.query.limit || 20,
        offset = 0,
        query = req.query.q;

    let where = {};
    if (query.length > 0) {
        where = {
            name: {
                $like: query +'%'
            },
        };
    }

    models.Tag
        .findAll({
            offset: offset,
            limit: limit,
            where: where
        })
        .then(result => {
            let json = [];

            result.forEach((row) => {
                json.push({
                    name: row.name,
                    value: row.id,
                });
            });

            res.json({
                items: json
            });
        })
        .catch(next);
});

router.get('/add', (req, res, next) => {
    let name = req.param('name') || '';

    res.render('tags/add', {
        name: name,
    });
});

router.post('/add', (req, res, next) => {
    let models = req.app.get('models'),
        name = req.param('name') || '';

    if (name.length === 0) {
        req.flash('error', 'Name is empty');
        return res.redirect('/tags/add');
    }

    models.Tag
        .build({
            name: name
        })
        .save()
        .then(() => {
            req.flash('success', 'Tag has added successfully!');
            return res.redirect('/tags/add');
        })
        .catch(error => {
            req.flash('error', error);
            return res.redirect('/tags/add');
        });
});

router.get('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Tag
        .findById(id)
        .then(tag => {
            if (tag === null) {
                req.status = 404;
                return next();
            }

            res.render('tags/edit', {
                tag: tag.dataValues,
            });
        })
        .catch(next);
});

router.post('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id'),
        name = req.param('name');

    models.Tag
        .findById(id)
        .then(tag => {
            if (tag === null) {
                req.status = 404;
                return next();
            }
            models.Tag
                .update({
                    name: name
                }, {
                    where: {
                        id: tag.get('id')
                    }
                })
                .then(() => {
                    req.flash('success', 'Tag has edited successfully!');
                    return res.redirect(req.originalUrl);
                })
                .catch(error => {
                    req.flash('error', error);
                    return res.redirect(req.originalUrl);
                });
        })
        .catch(next);
});

router.get('/:id(\\d+)/remove', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Tag
        .destroy({
            where: {
                id: id
            }
        })
        .then((removed) => {
            if (removed) {
                req.flash('success', 'Tag has removed successfully!');
            }
            return res.redirect('/tags');
        })
        .catch(error => {
            req.flash('error', error);
            return res.redirect('/tags');
        });
});

module.exports = router;
